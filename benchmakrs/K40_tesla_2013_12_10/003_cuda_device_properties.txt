   --- General Information for device ---   0
Name:Tesla K40m
Compute capability: 3.5
Clock rate: 875500
Device copy overlap: Enabled
Kernel execution timeout: Disabled
   --- Memory Information for device --- 0
Total global mem: 12079136768
Total constant Mem: 65536
Max mem pitch: 2147483647
Texture Alignment: 512
   --- MP Information for device --- 0
Multiprocessor count: 15
Shared mem per mp: 49152
Registers per mp: 65536
Threads in warp: 32
Max threads per block: 1024
Max thread dimensions: 1024,1024,64
Max grid dimensions: 2147483647,65535,65535

   --- General Information for device ---   1
Name:Tesla K40m
Compute capability: 3.5
Clock rate: 875500
Device copy overlap: Enabled
Kernel execution timeout: Disabled
   --- Memory Information for device --- 1
Total global mem: 12079136768
Total constant Mem: 65536
Max mem pitch: 2147483647
Texture Alignment: 512
   --- MP Information for device --- 1
Multiprocessor count: 15
Shared mem per mp: 49152
Registers per mp: 65536
Threads in warp: 32
Max threads per block: 1024
Max thread dimensions: 1024,1024,64
Max grid dimensions: 2147483647,65535,65535

