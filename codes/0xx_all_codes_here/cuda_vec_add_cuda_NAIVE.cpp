//===========================================//
// Vector manipulations: C = (A*B*B) + (A+B) //
//===========================================//

//  1 --> Includes

#include <iostream>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <time.h>

using namespace std;

//  2 --> Functions

void CleanupResources(void);
void RandomInit(double*, int);

//  3 --> CUDA Kernel code

const int K_MAX = 22;
clock_t begin_time;
clock_t end_time;

__global__ void VecAdd(const double* A, const double* B, double* C, int m, int n)
{
  int row = blockDim.y * blockIdx.y + threadIdx.y;
  int col = blockDim.x * blockIdx.x + threadIdx.x; 

  if ((row < m) && (col < n))
  {
    for (int k = 1; k < K_MAX; k++)
	{ 
	  C[n*row+col] = sin(static_cast<double>(k)) * 
	                 cos(static_cast<double>(k)) *
	                 A[n*row+col]*B[n*row+col]*B[n*row+col] +
					 A[n*row+col]+B[n*row+col]; 
    }
 
    for (int k = 1; k < K_MAX; k++)
	{ 
	  C[n*row+col] = A[n*row+col]*B[n*row+col]*B[n*row+col] + A[n*row+col] + B[n*row+col]; 
    }
  }

}

//  4 --> Global variables
 
  double* h_A;
  double* h_B;
  double* h_C;
  double* d_A;
  double* d_B;
  double* d_C;

// 5 --> Host code

int main()
{
  //  1 --> local parameters

  const int iBench_MAX = 100000;
 
  //  2 --> main benchmark loop
   
  const int coef = 6;
  const int m_row = coef * 1024; 
  const int n_col = coef * 1024;
  const int total_elem = m_row * n_col;

  size_t size = m_row * n_col * sizeof(double);
   
  //  3 --> Allocate input vectors h_A and h_B in host memory
 
  h_A = new double [size];
  if (h_A == 0) 
  { CleanupResources(); }
 
  h_B = new double [size];
  if (h_B == 0) 
  { CleanupResources(); }
 
  h_C = new double [size];
  if (h_C == 0)
  { CleanupResources(); }
    
  //  4 --> Initialize input vectors

  cout << "  0 --> initialize the vectors -->"; 

  begin_time = clock();

  RandomInit(h_A, total_elem);
  RandomInit(h_B, total_elem);

  end_time = clock();

  double time_spent1 = static_cast<double>(end_time-begin_time)/CLOCKS_PER_SEC;

  cout << " cpu time used --> " << time_spent1 << endl;

  for(int iBench = 1; iBench <= iBench_MAX; iBench++)
  {
	cout << endl; 
	cout << "=====================================================================" << endl; 
    cout << endl;
	cout << "  1 --> cuda vector addition --> start main bench loop " << endl;
 
    //  5 --> Allocate vectors in device memory
 
	cout << "  2 --> cuda memory allocation " << endl;

    cudaMalloc((void**)&d_A, size);
    cudaMalloc((void**)&d_B, size);
    cudaMalloc((void**)&d_C, size);

    //  6 --> Copy vectors from host memory to device memory
 
	cout << "  3 --> copy data form host to cuda device";

	begin_time = clock();

    cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);

	end_time = clock();

	double time_spent2 = static_cast<double>(end_time-begin_time)/CLOCKS_PER_SEC;

    cout << " --> cpu time used --> " << time_spent2 << endl;

    //  7 --> Invoke kernel
 
	cout << "  4 --> invoke the cuda kernel";  

    const int threadsPerBlock = 32; // maximum for 2D block of threads 
    const int blocksPerGridCol = ceil(static_cast<double>(n_col)/threadsPerBlock);
	const int blocksPerGridRow = ceil(static_cast<double>(m_row)/threadsPerBlock);
    
    dim3 dimBlock(blocksPerGridCol, blocksPerGridRow, 1);
	dim3 dimGrid(threadsPerBlock, threadsPerBlock, 1);

	begin_time = clock();

	VecAdd<<<dimBlock, dimGrid>>>(d_A, d_B, d_C, m_row, n_col);
   
	end_time = clock();

    double time_spent3 = static_cast<double>(end_time-begin_time)/CLOCKS_PER_SEC;

	cout << " --> cpu time used --> " << time_spent3 << endl; 

    //  8 --> Copy result from device memory to host memory
    //        h_C contains the result in host memory

	cout << "  5 --> copy data from cuda device to host";

	begin_time = clock();

    cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);
    
	end_time = clock();

    double time_spent4 = static_cast<double>(end_time-begin_time)/CLOCKS_PER_SEC;

	cout << " --> cpu time used --> " << time_spent4 << endl;

    //  9 --> Verify result
   
	cout << "  6 --> verify the results --> starts " << endl;

	begin_time = clock();

	for ( int k = 0; k <= K_MAX; k++)
	{
      for (int i = 0; i < total_elem; ++i)
	  {
      
		double sum_tmp = sin(static_cast<double>(k)) * 
	                     cos(static_cast<double>(k)) *
	                     h_A[i]*h_B[i]*h_B[i] +
					     h_A[i]+h_B[i];

        double sum = h_A[i]*h_B[i]*h_B[i] + h_A[i] + h_B[i];

	    if (fabs(h_C[i] - sum) > 1e-10)
        { cout << " Error for index --> " << i << " --> rubbish --> " << sum_tmp << endl; break; }
        else if ((i == total_elem-1) && (k == K_MAX) && (fabs(h_C[i] - sum) <= 1e-10))
	    { cout << endl << "  --> Okay for --> " << iBench << " --> rubbish --> " << sum_tmp << endl << endl; }
      }
	}

	end_time = clock();

    double time_spent5 = static_cast<double>(end_time-begin_time)/CLOCKS_PER_SEC;

	cout << "  7 --> verify the results --> ends " << endl;
	cout << "  8 --> cpu time used --> " << time_spent5 << endl;

	// 10. clean CUDA RAM 

	cout << "  9 --> free up cuda RAM " << endl;

	if (d_A)
    { cudaFree(d_A); }
    if (d_B)
    { cudaFree(d_B); }
    if (d_C)
    { cudaFree(d_C); }

	cout << " 10 --> reset cuda device " << endl; 

	cudaDeviceReset();

	cout << " 11 --> total CUDA time     --> " << time_spent2+time_spent3+time_spent4 << endl;
	cout << " 12 --> total HOST CPU time --> " << time_spent5 << endl;
	cout << " 13 --> loop end and restart " << endl;
	
  } // benchmark loop end

  // 11 --> free up ALL RAM --> host + CUDA

  CleanupResources(); 
}

//=============================================================================
// functions definition
//=============================================================================

//  1 --> void CleanupResources(void)

void CleanupResources(void)
{
    // Free device memory
  
    if (d_A)
    { cudaFree(d_A); }
    if (d_B)
    { cudaFree(d_B); }
    if (d_C)
    { cudaFree(d_C); }

    // Free host memory
  
    if (h_A)
    { delete [] h_A; }
    if (h_B)
    { delete [] h_B; }
    if (h_C)
    { delete [] h_C; }
        
    cudaDeviceReset();
}

// 2 --> Allocates an array with random float entries.

void RandomInit(double* data, int n)
{
  for (int i = 0; i < n; ++i)
  { data[i] = rand() / static_cast<double>(RAND_MAX); }
}

//================//
// End of program //
//================//