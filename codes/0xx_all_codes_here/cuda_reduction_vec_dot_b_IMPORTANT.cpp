
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <ctime>

using namespace std;

// 1. Definitions

#define imin(a,b) (a<b?a:b)
#define sum_squares(x) (x*(x+1)*(2*x+1)/6)

// 2. Global parameters

const int I_MAX_CUDA       = 50;
const int I_MAX            = 100;
const int COEF             = 60;
const int N                = COEF*1024*1024;
const int threadsPerBlock  = 1024;
const int maxBlocksPerGrid = 65535;
const int blocksPerGrid    = imin(maxBlocksPerGrid, (N+threadsPerBlock-1)/threadsPerBlock);

// 3. The CUDA Kernel

__global__ void dotCUDAKernel(double *a, double *b, double *c)
{
  __shared__ double cache[threadsPerBlock];
  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  int cacheIndex = threadIdx.x;
  double temp = 0;

  for(int i_cuda = 0; i_cuda < I_MAX_CUDA; i_cuda++)
  { // bench for start

    while (tid < N)
    {
      temp = temp + a[tid] * b[tid];
      tid = tid + blockDim.x * gridDim.x;
    }

    // set the cache values

    cache[cacheIndex] = temp;

    // synchronize threads in this block

    __syncthreads();

    // for reductions, threadsPerBlock must be a power of 2
    // because of the following code

    int i = blockDim.x/2;

    while (i != 0) 
    {
      if (cacheIndex < i)
      { cache[cacheIndex] += cache[cacheIndex + i]; }
      __syncthreads();
      i /= 2;
    }

    if (cacheIndex == 0)
    { c[blockIdx.x] = cache[0]; }

  } // bench for end

}

// 4. The main function

int main() 
{
  // 0. Introduction
 
  cout << endl;
  cout << "====================================================================" << endl;
  cout << endl;
  cout << " A. CUDA Reduction: Dot Product"                      << endl;
  cout << " B. dot_result = Dot[A,B], 50 times"                  << endl;
  cout << " C. The dimension of each vector is: " << N           << endl; 
  cout << " D. We need 1 GByte GPU RAM"                          << endl;
  cout << " E. Threads Per Block equals to: " << threadsPerBlock << endl; 
  cout << " F. Blocks Per Grid equals to: "   << blocksPerGrid   << endl; 
  cout << " G. Benchmark is executed times equals to: " << I_MAX << endl;
  cout << endl;
  cout << "====================================================================" << endl;
  cout << endl;
  cout << " Enter a integer for the benchmark to start: ";
  int sentinel;
  cin >> sentinel;
  cout << endl;

  // 1. Local variables

  double *a;
  double *b;
  double c;
  double *partial_c;
  double *dev_a;
  double *dev_b;
  double *dev_partial_c;
  cudaError_t cudaStatus;
  clock_t begin_time;
  clock_t end_time;
  double time_spent;
  cudaEvent_t cuda_start;
  cudaEvent_t cuda_stop;  

  // 2. Allocate memory on the CPU side

  a = new double [N];
  b = new double [N];
  partial_c = new double [blocksPerGrid];

  // 3. Allocate the memory on the GPU
  
  cudaStatus = cudaMalloc((void**)&dev_a, N*sizeof(double));
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc for dev_a failed!" << endl; 
    return (-1); }
  else
  { cout << "  1 --> Success --> cudaMalloc for dev_a" << endl; }

  cudaStatus = cudaMalloc((void**)&dev_b, N*sizeof(double));
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc for dev_b failed!" << endl; 
    return (-1); }
  else
  { cout << "  2 --> Success --> cudaMalloc for dev_b" << endl; }

  cudaStatus = cudaMalloc((void**)&dev_partial_c, blocksPerGrid*sizeof(double));
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc for dev_partial_c failed!" << endl; 
    return (-1); }
  else
  { cout << "  3 --> Success --> cudaMalloc for dev_partial_c" << endl; }

  // 4. Fill in the host memory with data

  for (int i = 0; i < N; i++) 
  {
    a[i] = static_cast<double>(i);
    b[i] = static_cast<double>(i*2);
  }

  // 5. Copy the arrays 'a' and 'b' to the GPU
  
  cudaStatus=cudaMemcpy(dev_a, a, N*sizeof(double), cudaMemcpyHostToDevice);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMemcpy for a to dev_a failed!" << endl; 
    return (-1); }
  else
  { cout << "  4 --> Success --> cudaMemcpy for a to dev_a" << endl; }

  cudaMemcpy(dev_b, b, N*sizeof(double), cudaMemcpyHostToDevice);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMemcpy for b to dev_b failed!" << endl; 
    return (-1); }
  else
  { cout << "  5 --> Success --> cudaMemcpy for b to dev_b" << endl; }

//=======================================================================================
// 6. Main benchmark
//=======================================================================================

for (int i = 0; i < I_MAX; i++)
{
  begin_time = clock();
  cudaEventCreate(&cuda_start);
  cudaEventCreate(&cuda_stop);
  cudaEventRecord(cuda_start, 0);

  cout << " ===============================================> Benchmark counter ===> " 
       << i << endl;

  // 7. Launch the CUDA kernel function

  dotCUDAKernel<<<blocksPerGrid, threadsPerBlock>>>(dev_a, dev_b, dev_partial_c);

  // 8. cudaDeviceSynchronize waits for the kernel to finish, and returns
  //    any errors encountered during the launch.

  cudaStatus = cudaDeviceSynchronize();
  if (cudaStatus != cudaSuccess) 
  { cout << " Error! cudaDeviceSynchronize returned error code "
         << cudaStatus << " after launching addVecKernel!" << endl;
    return (-1); }
  else
  { cout << "  6 --> Success --> cudaDeviceSynchronize()" << endl; }

  // 9. Copy the array 'c' back from the GPU to the CPU
  
  cudaStatus = cudaMemcpy(partial_c, 
                          dev_partial_c,
                          blocksPerGrid*sizeof(double),
                          cudaMemcpyDeviceToHost);

  if (cudaStatus != cudaSuccess) 
  { cout << "Error! cudaMemcpy for dev_partial_c to partial_c failed!" << endl;
    return (-1); }
  else
  { cout << "  7 --> Success --> cudaMemcpy for dev_partial_c to partial_c" << endl; }

  end_time = clock();
  cudaEventRecord(cuda_stop, 0);
  cudaEventSynchronize(cuda_stop);

  time_spent = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
  float time_gpu;
  cudaEventElapsedTime(&time_gpu, cuda_start, cuda_stop);  
  time_gpu = time_gpu/CLOCKS_PER_SEC;

  cout << " ==========================================> CPU Time Used ============> " 
       << time_spent << endl;
  cout << " ==========================================> GPU Time Used ============> "
       << time_gpu << endl;
}

  // 9. Finish up on the CPU side
    
  c = 0;
    
  for (int i = 0; i < blocksPerGrid; i++)
  { c = c + partial_c[i]; }
    
  cout << " --> GPU value = " << c  << endl;
  cout << " --> CPU value = " << 2*sum_squares(static_cast<double>(N-1)) << endl;

  // 10. Free memory on the GPU side
   
  cudaStatus = cudaFree(dev_a); 
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaFree for dev_a failed!" << endl; 
    return (-1); }
  else
  { cout << "  8 --> Success --> cudaFree for dev_a" << endl; }

  cudaStatus = cudaFree(dev_b);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaFree for dev_b failed!" << endl; 
    return (-1); }
  else
  { cout << "  9 --> Success --> cudaFree for dev_b" << endl; }
 
  cudaFree(dev_partial_c);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaFree for dev_partial_c failed!" << endl; 
    return (-1); }
  else
  { cout << " 10 --> Success --> cudaFree dev_partial_c" << endl; }

  // 11. Free memory on the CPU side
 
  delete [] a;
  delete [] b;
  delete [] partial_c;

  // 12. Sentinel

  cout << " Enter an integer to exit:"; 
  cin >> sentinel;
  cout << endl;

  // 13. Exit

  return 0;
}

//=============//
// End of code //
//=============//