
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <ctime>

using namespace std;

// A. CUDA kernel 
//    The most general kernel for vector addition

__global__ void addVecKernel(const double *a, const double *b, double *c, const int dim)
{
  int tid;
  const int I_MAX_CUDA = 100;  

  for (int i = 0; i < I_MAX_CUDA; i++)
  {

    tid = threadIdx.x + blockIdx.x * blockDim.x;
    while (tid < dim)
    { c[tid] = a[tid] + b[tid];
      tid = tid + blockDim.x * gridDim.x; }
  }
}

// B. The main function

int main()
{
  // 1. Local variables and parameters
  
  const int    COEF          = 5;
  const int    DIMEN         = COEF * pow(1024,2.0);
  const int    I_MAX_THREADS = 1024;
  const double ERROR_LIM     = 10e-8;
  double *a_host = new double [DIMEN];
  double *b_host = new double [DIMEN];
  double *c_host = new double [DIMEN];
  double *a_dev;
  double *b_dev;
  double *c_dev;
  cudaError_t cudaStatus;   
  double tmp_val;  
  int numOfBlocks;
  int numOfThreadsPerBlock;
  int bench_count;
  clock_t begin_time;
  clock_t end_time;
  double time_spent;
  int sentinel;
  int step_i;

  // 1B. Preface:
 
  cout << "====================================================================" << endl;
  cout << "  1 --> Benchmark Name: Vector Addition"                              << endl;
  cout << "  2 --> C[i] = A[i] + B[i], for  0 < i < DIMEN, 100 times"            << endl;
  cout << "  3 --> The size of each vector is 5*pow(1024,2)"                     << endl;
  cout << "  4 --> The GPU RAM needed is 120 MBytes"                             << endl;
  cout << "  5 --> The number of blocks is fixed to 65535"                       << endl;
  cout << "  6 --> The number of threads per block varies from 1 to 1024"        << endl;  
  cout << "====================================================================" << endl;
  cout << endl;
  cout << "  Enter the threads step (10 <= step <= 100): ";
  cin >> step_i;
  cout << endl;
  cout << "  Enter an integer to start the benchmark: ";
  cin >> sentinel;
  cout << endl;

  // 2. Initialization vectors in host device

  for(int i = 0; i < DIMEN; i++)
  {
    a_host[i] = static_cast<double>(i);
    b_host[i] = static_cast<double>(i+1);
    c_host[i] = static_cast<double>(0);
  } 

  // 3. Choose which GPU to run on.
  //    Change this on a multi-GPU system.

  cudaStatus = cudaSetDevice(0);
  if (cudaStatus != cudaSuccess)
  { cout << "cudaSetDevice failed! Do you have a CUDA-capable GPU installed?" << endl; 
    return (-1); }
  else
  { cout << "  1 --> Success --> cudaSetDevice(0)" << endl; }

  // 4. Allocate memory in CUDA device

  cudaStatus = cudaMalloc((void**)& a_dev, DIMEN*sizeof(double)); 
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc failed for variable: a_dev" << endl; 
    return (-1); }
  else
  { cout << "  2 --> Success --> cudaMalloc --> a_dev" << endl; }

  cudaStatus = cudaMalloc((void**)& b_dev, DIMEN*sizeof(double));
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc failed for variable: b_dev" << endl; 
    return (-1); }
  else
  { cout << "  3 --> Success --> cudaMalloc --> b_dev" << endl; }

  cudaStatus = cudaMalloc((void**)& c_dev, DIMEN*sizeof(double));
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc failed for variable: c_dev" << endl; 
    return (-1); }
  else
  { cout << "  4 --> Success --> cudaMalloc --> c_dev" << endl; }

  // 5. Copy data from host to CUDA device
  
  cudaStatus = cudaMemcpy(a_dev, a_host, DIMEN*sizeof(double), cudaMemcpyHostToDevice);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMemcpy failed for variable: a_host to a_dev" << endl; 
    return (-1); }
  else
  { cout << "  5 --> Success --> cudaMemcpy --> a_host to a_dev" << endl; }

  cudaStatus = cudaMemcpy(b_dev, b_host, DIMEN*sizeof(double), cudaMemcpyHostToDevice);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMemcpy failed for variable: b_host to b_dev" << endl; 
    return (-1); }
  else
  { cout << "  6 --> Success --> cudaMemcpy --> b_host to b_dev" << endl; }

//=================================================================================================
// The benchmark
//=================================================================================================

  // 6. Add vectors in parallel.
  //    Launch a kernel on the GPU with one thread for each element.

bench_count = 0;

for (int i = 1; i <= I_MAX_THREADS; i=i+step_i) 
{ 
  numOfBlocks = 65535;
  numOfThreadsPerBlock = i; 
  bench_count++;  
   
  cout << "=========> A ==> Benchmark counter is --------------------> " << bench_count << endl;
  cout << "=========> B ==> Number of Threads is --------------------> " << i << endl; 

  begin_time = clock();

  addVecKernel<<<numOfBlocks, numOfThreadsPerBlock>>>(a_dev, b_dev, c_dev, DIMEN);

  // 7. cudaDeviceSynchronize waits for the kernel to finish, and returns
  //    any errors encountered during the launch.
    
  cudaStatus = cudaDeviceSynchronize();
  if (cudaStatus != cudaSuccess) 
  { cout << " Error! cudaDeviceSynchronize returned error code "
         << cudaStatus << " after launching addVecKernel!" << endl;
    return (-1); }
  else
  { cout << "  7 --> Success --> cudaDeviceSynchronize()" << endl; }

  // 8. Copy data from CUDA device to host

  cudaStatus = cudaMemcpy(c_host, c_dev, DIMEN*sizeof(double), cudaMemcpyDeviceToHost);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMemcpy failed for variable: c_dev to c_host" << endl; 
    return (-1); }
  else
  { cout << "  8 --> Success --> cudaMemcpy --> c_dev to c_host" << endl; }

  end_time = clock();

  time_spent = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
  
  cout << "=========> C ==> Time Needed -----------------------------> " << time_spent << endl;

  // 9. Test the results
  
  for (int i = 0; i < DIMEN; i++)
  { 
    tmp_val = c_host[i] - (a_host[i]+b_host[i]); 
    if (fabs(tmp_val) > ERROR_LIM)
    { cout << "Error! For index " << i << " equals to " << tmp_val << endl;
      return (-1); }
    else if (i == DIMEN-1)
    { cout << "  9 --> The results are okay!" << endl; }
  }

}
//=================================================================================================
// End of benchmark
//=================================================================================================

  // 10. cudaDeviceReset must be called before exiting in order for profiling and
  //    tracing tools such as Nsight and Visual Profiler to show complete traces.
 
  cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess) 
  { cout << " Error! cudaDeviceReset failed!" << endl;
    return (-1); }
  else
  { cout << " 10 --> Success --> cudaDeviceReset" << endl; }

  // 11. Some output

  cout << " 11 --> a_host[1]       = " << a_host[1]       << endl;
  cout << " 12 --> b_host[1]       = " << b_host[1]       << endl;
  cout << " 13 --> c_host[1]       = " << c_host[1]       << endl;
  cout << " 14 --> a_host[dimen-1] = " << a_host[DIMEN-1] << endl;
  cout << " 15 --> b_host[dimen-1] = " << b_host[DIMEN-1] << endl;
  cout << " 16 --> c_host[dimen-1] = " << c_host[DIMEN-1] << endl;

  // 12. Free RAM on host device

  delete [] a_host;
  delete [] b_host;
  delete [] c_host;

  // 13. Free RAM on CUDA device

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);

  // 14. Exit with success
  
  cout << "  Enter an integer to exit:";
  cin >> sentinel;

  return 0;
}

//=============//
// End of code //
//=============//