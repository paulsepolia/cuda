
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <ctime>

using namespace std;

// 1. Global parameters

const int I_MAX = 200;
const int COEF = 220;
const int SIZE = COEF * 1024 * 1024;

// 2. Function: cuda_malloc_test 

float cuda_malloc_test(int size, bool up)
{
  cudaEvent_t start;
  cudaEvent_t stop;
  int *a;
  int *dev_a;
  float elapsedTime;
  cudaError_t cudaStatus;
 
  // a.

  cout << endl;
  cout << " Inside --------------------> cuda_malloc_test " << endl;

  cudaStatus = cudaEventCreate(&start);
  if (cudaStatus != cudaSuccess)
  { cout << " 1 --> Error! cudaEventCreate(&start) failed!" << endl; 
    return (-1); }

  // b.

  cudaStatus = cudaEventCreate(&stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 2 --> Error! cudaEventCreate(&stop) failed!" << endl; 
    return (-1); }

  // c.

  a = (int*)malloc(size* sizeof(*a));
  cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(*dev_a));
  if (cudaStatus != cudaSuccess)
  { cout << " 3 --> Error! cudaMalloc for dev_a failed!" << endl; 
    return (-1); }

  // d.

  cudaStatus = cudaEventRecord(start, 0);
  if (cudaStatus != cudaSuccess)
  { cout << " 4 --> Error! cudaEventRecord(start,0) failed!" << endl; 
    return (-1); }

  // e.

  for(int i = 0; i < I_MAX; i++)
  {
    if(up)
    { cudaStatus = cudaMemcpy(dev_a, a, size* sizeof(*dev_a), cudaMemcpyHostToDevice); 
      if (cudaStatus != cudaSuccess)
      { cout << " 5 --> Error! cudaMemcpy from host to device, a -> dev_a, failed!" << endl; 
        return (-1); }
    }
    else
    { cudaStatus = cudaMemcpy(a, dev_a, size* sizeof(*dev_a), cudaMemcpyDeviceToHost); 
      if (cudaStatus != cudaSuccess)
      { cout << " 6 --> Error! cudaMemcpy from host to device, a -> dev_a, failed!" << endl; 
        return (-1); }
    }
  }

  // f.

  cudaStatus = cudaEventRecord(stop, 0);
  if (cudaStatus != cudaSuccess)
  { cout << " 7 --> Error! cudaEventRecord(stop, 0), failed!" << endl; 
    return (-1); }

  // g.

  cudaStatus = cudaEventSynchronize(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 8 --> Error! cudaEventSynchronize(stop), failed!" << endl; 
    return (-1); }

  // h.

  cudaStatus = cudaEventElapsedTime(&elapsedTime, start, stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 9 --> Error! cudaEventElapsedTime(&elapsedTime, start, stop), failed!" << endl; 
    return (-1); }

  // i.

  free(a);
  
  cudaStatus = cudaFree(dev_a);
  if (cudaStatus != cudaSuccess)
  { cout << " 10 --> Error! cudaFree(dev_a), failed!" << endl; 
    return (-1); }

  // j.
  
  cudaStatus = cudaEventDestroy(start);
  if (cudaStatus != cudaSuccess)
  { cout << " 11 --> Error! cudaEventDestroy(start), failed!" << endl; 
    return (-1); }
 
  // k.

  cudaStatus = cudaEventDestroy(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 12 --> Error! cudaEventDestroy(stop), failed!" << endl; 
    return (-1); }
 
  // l.

  cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess) 
  { cout << " 13 --> Error! cudaDeviceReset() failed!" << endl;
    return (-1); }
  
  return (elapsedTime/CLOCKS_PER_SEC);
}

// 3. Function: cuda_host_alloc_test

float cuda_host_alloc_test(int size, bool up)
{
  cudaEvent_t start;
  cudaEvent_t stop;
  int *a;
  int *dev_a;
  float elapsedTime;
  cudaError_t cudaStatus;

  // a.

  cout << endl;
  cout << " Inside --------------------> cuda_host_alloc_test" << endl;

  cudaStatus = cudaEventCreate(&start);
  if (cudaStatus != cudaSuccess)
  { cout << " 14 --> Error! cudaEventCreate(&start) failed!" << endl; 
    return (-1); }
  
  // b.

  cudaStatus = cudaEventCreate(&stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 15 --> Error! cudaEventCreate(&stop) failed!" << endl; 
    return (-1); }

  // c.

  cudaStatus = cudaHostAlloc((void**)&a, size*sizeof(*a), cudaHostAllocDefault);
  if (cudaStatus != cudaSuccess)
  { cout << " 16 --> Error! cudaHostAlloc for a failed!" << endl; 
    return (-1); }

  // d.

  cudaStatus = cudaMalloc((void**)&dev_a, size*sizeof(*dev_a));
  if (cudaStatus != cudaSuccess)
  { cout << " 17 --> Error! cudaMalloc for dev_a failed!" << endl; 
    return (-1); }

  // e.
  
  cudaStatus = cudaEventRecord(start,0);
  if (cudaStatus != cudaSuccess)
  { cout << " 18 --> Error! cudaEventRecord(start, 0), failed!" << endl; 
    return (-1); }

  // f.

  for(int i = 0; i < I_MAX; i++)
  {
    if(up)
    { cudaStatus = cudaMemcpy(dev_a, a, size* sizeof(*a), cudaMemcpyHostToDevice);
      if (cudaStatus != cudaSuccess)
      { cout << " 19 --> Error! cudaMemcpy from a to dev_a, failed!" << endl; 
        return (-1); }
    }
    else
    { cudaStatus = cudaMemcpy(a, dev_a, size* sizeof(*a), cudaMemcpyDeviceToHost); 
      if (cudaStatus != cudaSuccess)
      { cout << " 20 --> Error! cudaMemcpy from dev_a to a, failed!" << endl; 
        return (-1); }
    }
  }

  // f.

  cudaStatus = cudaEventRecord(stop, 0);
  if (cudaStatus != cudaSuccess)
  { cout << " 21 --> Error! cudaEventRecord(stop, 0), failed!" << endl; 
    return (-1); }

  // g.

  cudaStatus = cudaEventSynchronize(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 22 --> Error! cudaEventSynchronize(stop), failed!" << endl; 
    return (-1); }

  // h.

  cudaStatus = cudaEventElapsedTime(&elapsedTime, start, stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 23 --> Error! cudaEventElapsedTime(&elapsedTime, start, stop), failed!" << endl; 
    return (-1); }

  // i.
  
  cudaFreeHost(a);  
  if (cudaStatus != cudaSuccess)
  { cout << " 24 --> Error! cudaFreeHost(a), failed!" << endl; 
    return (-1); }

  // j.

  cudaStatus = cudaFree(dev_a);
  if (cudaStatus != cudaSuccess)
  { cout << " 25 --> Error! cudaFree(dev_a), failed!" << endl; 
    return (-1); }

  // k.
  
  cudaStatus = cudaEventDestroy(start);
  if (cudaStatus != cudaSuccess)
  { cout << " 26 --> Error! cudaEventDestroy(start), failed!" << endl; 
    return (-1); }
 
  // l.

  cudaStatus = cudaEventDestroy(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " 27 --> Error! cudaEventDestroy(stop), failed!" << endl; 
    return (-1); }
 
  // m.

  cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess) 
  { cout << " 28 --> Error! cudaDeviceReset() failed!" << endl;
    return (-1); }
  
  return (elapsedTime/CLOCKS_PER_SEC);
}

// 4. The main function


int main()
{
  // 1. Introduction

const int K_MAX = 100;

int sentinel;

cout << "====================================================================" << endl;
cout << " A. I copy Gbytes   = " << SIZE*sizeof(int)/pow(1024,3.0)             << endl;
cout << " B. I do that times = " << I_MAX                                      << endl;
cout << " C. I use 'cudaHostAlloc' and 'malloc' "                              << endl;   
cout << "====================================================================" << endl; 

for (int k = 1; k <= K_MAX; k++)
{ 
  cout << endl;
  if (k==1) 
  { cout << " Enter an integer to start the benchmark: ";
    cin >> sentinel; }

  cout << endl;
  cout << " ---------------------------------------------------------------------> " << k   
       << endl;

  // 2. Local variables

  float elapsedTime;
  double GB = SIZE*sizeof(int)/pow(1024,3.0);

  // 3. CUDA malloc up

  elapsedTime = cuda_malloc_test(SIZE, true);

  cout << endl;
  cout << " D-1. Time using cudaMalloc and malloc         = " 
       << elapsedTime << endl;
  cout << " D-2. GBytes/s during copy up                  = " 
       << I_MAX*GB/elapsedTime << endl;

  // 4. CUDA malloc down

  elapsedTime = cuda_malloc_test(SIZE, false);

  cout << endl;
  cout << " D-3. Time using cudaMalloc and malloc         = "
       << elapsedTime << endl;
  cout << " D-4. GBytes/s during copy down                = "
       << I_MAX*GB/elapsedTime << endl;

  // 5. CUDA host alloc up

  elapsedTime = cuda_host_alloc_test(SIZE, true);

  cout << endl;
  cout << " D-5. Time using cudaHostAlloc and cudaMalloc  = " 
       << elapsedTime << endl;
  cout << " D-6. GBytes/s during copy up                  = " 
       << I_MAX*GB/elapsedTime << endl;
   
  // 6. CUDA host alloc down

  elapsedTime = cuda_host_alloc_test(SIZE, false);

  cout << endl;
  cout << " D-7. Time using cudaHostAlloc and cudaMalloc  = " 
       << elapsedTime << endl;
  cout << " D-8. GBytes/s during copy down                = " 
       << I_MAX*GB/elapsedTime << endl;

}
  // exit

  cout << endl;
  cout << " Enter an integer to exit: ";
  cin >> sentinel;

  return 0;

}

//=============//
// End of code //
//=============//