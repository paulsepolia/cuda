
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <ctime>

using namespace std;

// A. The CUDA Dot[Matrix,Matrix] Kernel

__global__ void MatrixMulKernel(const double* d_M, const double* d_N, double* d_P, int width)
{
  // 1 --> calculate the row index of the d_P and d_M

  int row = blockIdx.y * blockDim.y + threadIdx.y;

  // 2 --> calculate the column index of d_P and d_N

  int col = blockIdx.x * blockDim.x + threadIdx.x;

  // 3 --> main dor(matrix,matrix) kernel

  if ((row < width) && (col < width))  
  {
    double Pvalue = 0;

    // 4 --> each thread computes one element of the sub-matrix

    for (int k = 0; k < width; k++)
    {
      Pvalue = Pvalue + d_M[row*width+k] * d_N[k*width+col];
    }
  
    d_P[row*width+col] = Pvalue;
  }

} // end of cuda kernel

// B. The main function

int main()
{
  // 1 --> local parameters and variables

  cout << endl;
  int COEF;
  cout << " Enter the COEF, so as matrix dimension is COEF*128: ";
  cin >> COEF;
  cout << endl;
  
  const int I_BENCH_MAX = 10000000;
  int DIMEN       = COEF * 128;
  int TOTAL_ELEM  = DIMEN * DIMEN;
  size_t SIZE     = DIMEN * DIMEN * sizeof(double);   
  double* h_M = new double [TOTAL_ELEM];  // host device
  double* h_N = new double [TOTAL_ELEM];  // host device
  double* h_P = new double [TOTAL_ELEM];  // host device
  double* d_M;                            // cuda device
  double* d_N;                            // cuda device
  double* d_P;                            // cuda device
  cudaError_t cudaStatus;                          
  cudaEvent_t cuda_start;
  cudaEvent_t cuda_stop;

  //  2 --> initialize matrices in host
 
  cout << "--------------------------------------------------------------------" << endl;
  cout << " Matrix Multiplication Flatten in 1-D                               " << endl;
  cout << " Matrix Size is " << DIMEN << " x " << DIMEN                          << endl;
  cout << " I execute the benchmark " << I_BENCH_MAX << " times"                 << endl;
  cout << "--------------------------------------------------------------------" << endl;
  
  cout << endl;
  cout << " Enter an integer to start: ";
  int sentinel;
  cin >> sentinel;
  cout << endl << endl;

  cout << " 0 --> Matrices initialization in host" << endl; 
  
  for (int i = 0; i < DIMEN; i++)
  {
    for (int j = 0; j < DIMEN; j++)
	{
	  h_M[i*DIMEN+j] = rand()/static_cast<double>(RAND_MAX);
	  h_N[i*DIMEN+j] = rand()/static_cast<double>(RAND_MAX);
	  h_P[i*DIMEN+j] = static_cast<double>(0.0);
	}
  }

  //  3 --> benchmark start

  int BLOCK_DIMEN = 0; 

  for (int i = 1; i <= I_BENCH_MAX; i++)
  {
    cudaEventCreate(&cuda_start);
    cudaEventCreate(&cuda_stop);
    cudaEventRecord(cuda_start, 0);

    cout << " 1 --> Counter is ======================================" 
         << "==============================> " << i << endl;
    cout << " 2 --> CUDA Matrix Multiplication" << endl;
 
    //  4 --> allocate matrices in cuda device

    cudaStatus=cudaMalloc((void**)&d_M, SIZE);
    if (cudaStatus != cudaSuccess) { cout << " Error --> 1 " << endl; return (-1); }
   
    cudaMalloc((void**)&d_N, SIZE);
    if (cudaStatus != cudaSuccess) { cout << " Error --> 2 " << endl; return (-1); }
 
    cudaMalloc((void**)&d_P, SIZE);
    if (cudaStatus != cudaSuccess) { cout << " Error --> 3 " << endl; return (-1); }

    //  5 --> copy matrices from host memory to device memory

	cout << " 3 --> Copy RAM from HOST to CUDA" << endl; 

    cudaStatus = cudaMemcpy(d_M, h_M, SIZE, cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) { cout << " Error --> 4 " << endl; return (-1); }
  
    cudaStatus = cudaMemcpy(d_N, h_N, SIZE, cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) { cout << " Error --> 5 " << endl; return (-1); }
  
    //  6 --> set up cuda process grid

	cout << " 4 --> CUDA Kernel invoke" << endl;

    BLOCK_DIMEN = BLOCK_DIMEN + 1; // number of threads in each block dimension
    const int MAX_THREADS_PER_2D_BLOCK = 32;

    cout << " 5 --> 2D Block dimension is -------------------------> " 
         << BLOCK_DIMEN << ", " << BLOCK_DIMEN << endl;
    int NumBlocks = DIMEN/BLOCK_DIMEN;
    cout << " 6 --> Number of 2D Blocks is ------------------------> "
         << NumBlocks << endl;
    if(DIMEN % BLOCK_DIMEN) NumBlocks++;

    dim3 dimBlock(BLOCK_DIMEN, BLOCK_DIMEN);
	dim3 dimGrid(NumBlocks, NumBlocks);

	//  7 --> invoke cuda kernel

    //  7-a
	MatrixMulKernel<<<dimGrid, dimBlock>>>(d_M, d_N, d_P, DIMEN);

    //  7-b 
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) 
    { cout << " Error --> 6 --> cudaDeviceSynchronize returned error code "
           << cudaStatus << " after launching CUDA Kernel." << endl;
      return (-1); }

    //  8 --> Copy result from device memory to host memory
    //        h_P contains the result in host memory

	cout << " 7 --> Copy RAM from CUDA to HOST" << endl;

    cudaStatus = cudaMemcpy(h_P, d_P, SIZE, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) { cout << " Error --> 7 " << endl; return (-1); }

    //  9 --> Clean CUDA RAM 

	if (d_M)
    { cudaStatus=cudaFree(d_M); 
      if (cudaStatus != cudaSuccess) { cout << " Error --> 8 " << endl; return (-1); }}
    if (d_N)
    { cudaFree(d_N); 
      if (cudaStatus != cudaSuccess) { cout << " Error --> 9 " << endl; return (-1); }}
    if (d_P)
    { cudaFree(d_P); 
      if (cudaStatus != cudaSuccess) { cout << " Error --> 10 " << endl; return (-1); }}

    // 10 --> Timing

    cudaEventRecord(cuda_stop, 0);
    cudaEventSynchronize(cuda_stop);
    float time_gpu;
    cudaEventElapsedTime(&time_gpu, cuda_start, cuda_stop);
    time_gpu = time_gpu/CLOCKS_PER_SEC;
    
    // 11 --> Reset CUDA Device

 	cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) { cout << " Error --> 8 " << endl; return (-1); }
     
    cout << " 8 --> GPU time is -----------------------------------> " 
         << time_gpu << endl;

    // 12 --> Test the results

    double Pvalue_host = 0;
    int row_host = 1;
    int col_host = 2;

    for (int k = 0; k < DIMEN; k++)
    {
      Pvalue_host = Pvalue_host + h_M[row_host*DIMEN+k] * h_N[k*DIMEN+col_host];
    }
  
    cout << " 9 --> Host value[i] ---------------------------------> " 
         << h_P[row_host*DIMEN+col_host] << endl;
    cout << "10 --> CUDA value[i] ---------------------------------> " 
         << Pvalue_host << endl;
    cout << "11 --> test diff -------------------------------------> " 
         << (h_P[row_host*DIMEN+col_host]-Pvalue_host) << endl;
    cout << endl; 

    // 13 --> Reset the BLOCK_DIMEN 

    if (BLOCK_DIMEN%MAX_THREADS_PER_2D_BLOCK == 0) {BLOCK_DIMEN=1;} // return 

  } // benchmark loop end

  // 14 --> clean host RAM

  delete [] h_M;
  delete [] h_N;
  delete [] h_P;

  // 15 --> Exiting

  cout << endl;
  cout << "Enter an integer to exit: " << endl;
  cin >> sentinel;

  return 0;
}

//=============//
// End of code //
//=============//