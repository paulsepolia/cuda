//====================================================//
// Matrix addition: C = A + B                         //
// The matrices are represented as vectors in host    //
// The matrices are distributed as 2D objects in cuda //
//====================================================//

//  1 --> includes

#include <iostream>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <ctime>

using namespace std;

const int K_MAX = 24;
clock_t begin_time;
clock_t end_time;

//  2 --> cuda kernel code

__global__ void MatAdd(const double* A, const double* B, double* C, int m, int n)
{
  int row = blockDim.y * blockIdx.y + threadIdx.y;
  int col = blockDim.x * blockIdx.x + threadIdx.x;   

  if ((row < m) && (col < n))
  {
    // part --> 1 --> for delay

    for (int k = 1; k < K_MAX; k++)
	{ 
	  C[n*row+col] = sin(static_cast<double>(k)) * 
	                 cos(static_cast<double>(k)) *
	                 A[n*row+col]*B[n*row+col]*B[n*row+col] +
					 A[n*row+col]+B[n*row+col]; 
    }
 
    // part --> 2 --> matrix addition with some delay
   
    for (int k = 1; k < K_MAX; k++)
	{ 
	  C[n*row+col] = A[n*row+col] + B[n*row+col]; 
    }
  }
}

//  3 --> Host code

int main()
{
  //  1 --> local parameters and variables

  const int iBench_MAX = 100000;
  const int coef       = 6;
  const int m_row      = coef * 1024; 
  const int n_col      = coef * 1024;
  const int total_elem = m_row * n_col;
  size_t size          = m_row * n_col * sizeof(double); 
  
  double* h_A = new double [m_row*n_col];  // host
  double* h_B = new double [m_row*n_col];  // host
  double* h_C = new double [m_row*n_col];  // host

  double* d_A;  // cuda device
  double* d_B;  // cuda device
  double* d_C;  // cuda device

  //  2 --> initialize matrices in host
 
  cout << "--------------------------------------------------------------------" << endl;
  cout << " Matrix Addittion Flatten in 1-D                                    " << endl;
  cout << " Matrix Size is --> " << m_row << " x " << n_col                      << endl;
  cout << " There is a 24 times delay ==>                                      " << endl;
  cout << " I repeat the main loop 2*24 times                                  " << endl;
  cout << "--------------------------------------------------------------------" << endl;
  
  cout << "  1 --> matrix initialization in host"; 

  begin_time = clock();

  for (int i = 0; i < m_row; i++)
  {
    for (int j = 0; j < n_col; j++)
	{
	  h_A[i*n_col+j] = rand()/static_cast<double>(RAND_MAX);
	  h_B[i*n_col+j] = rand()/static_cast<double>(RAND_MAX);
	  h_C[i*n_col+j] = static_cast<double>(0.0);
	}
  }

  end_time = clock();
  double time_spent_1 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
  cout << " --> cpu time used --> " << time_spent_1 << endl; 

  //  3 --> benchmark start

  for(int iBench = 1; iBench <= iBench_MAX; iBench++)
  {
    cout << "  2 --> CUDA Matrix Addition" << endl;
 
    //  4 --> allocate matrices in cuda device

    cudaMalloc((void**)&d_A, size);
    cudaMalloc((void**)&d_B, size);
    cudaMalloc((void**)&d_C, size);

    //  5 --> copy matrices from host memory to device memory

	cout << "  3 --> copy RAM from HOST to CUDA"; 

	begin_time = clock();

    cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);

	end_time = clock();
    double time_spent_2 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
    cout << " --> cpu time used --> " << time_spent_2 << endl;

    //  6 --> set up cuda process grid

	cout << "  4 --> CUDA kernel invoke";

	begin_time = clock();

    const int threadsPerBlock = 32; // maximum for 2D block of threads 
    const int blocksPerGridCol = ceil(static_cast<double>(n_col)/threadsPerBlock);
	const int blocksPerGridRow = ceil(static_cast<double>(m_row)/threadsPerBlock);
    
    dim3 dimBlock(blocksPerGridCol, blocksPerGridRow, 1);
	dim3 dimGrid(threadsPerBlock, threadsPerBlock, 1);

	//  7 --> invoke cuda kernel

	MatAdd<<<dimBlock, dimGrid>>>(d_A, d_B, d_C, m_row, n_col);

	end_time = clock();
    double time_spent_3 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
    cout << " --> cpu time used --> " << time_spent_3 << endl;

    //  8 --> Copy result from device memory to host memory
    //        h_C contains the result in host memory

	cout << "  5 --> copy RAM from CUDA to HOST";

	begin_time = clock();

    cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);
    
	end_time = clock();
    double time_spent_4 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
    cout << " --> cpu time used --> " << time_spent_4 << endl;

    //  9 --> Verify result
   
    double value_local;

	cout << "  6 --> verify results";

	begin_time = clock();

	for (int k = 0; k <= K_MAX; k++)
	{
      for (int i = 0; i < m_row; i++)
	  {
	    for (int j = 0; j < n_col; j++)
	    {
		  double tmp = sin(static_cast<double>(k)) * 
	                   cos(static_cast<double>(k)) *
	                   h_A[n_col*i+j]*h_B[n_col*i+j]*h_B[n_col*i+j] +
					   h_A[n_col*i+j]+h_B[n_col*i+j];

	      double elem = h_A[i*n_col+j] + h_B[i*n_col+j];

		  value_local = fabs(h_C[i*n_col+j] - elem);

		  if ((value_local > 1e-10) && (k == K_MAX))
		  { cout << " Error for index --> " << i << "," << j << endl; break; }
		  else if ((i == 0) && (j == 0) && (k == K_MAX) )
		  { cout << " --> Okay for --> " << iBench << " --> rubbish --> " << tmp; }
	    }
	
	    if (value_local > 1e-10)
	    { cout << " Error outer " << endl; break; }
	  }
	}

	end_time = clock();
    double time_spent_5 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
    cout << " --> cpu time used --> " << time_spent_5 << endl;

	// 10 --> clean cuda RAM 

	if (d_A)
    { cudaFree(d_A); }
    if (d_B)
    { cudaFree(d_B); }
    if (d_C)
    { cudaFree(d_C); }

	// 11 --> reset cuda device

	cudaDeviceReset();

	cout << "====================================================================" << endl;
	cout << "  7 --> total CPU time --> " << time_spent_5 << endl;
	cout << "  8 --> total GPU time --> " << time_spent_2 + time_spent_3 + time_spent_4 << endl;
	cout << "====================================================================" << endl;

  } // benchmark loop end

  // 12 --> clean host RAM

  delete [] h_A;
  delete [] h_B;
  delete [] h_C;

  return 0;   
}

//================//
// End of program //
//================//