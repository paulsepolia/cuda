
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>

using namespace std;

// 1. CUDA kernel

__global__ void what_is_my_id(unsigned int* const block,
	                          unsigned int* const blockDimVal,
                              unsigned int* const thread,
							  unsigned int* const warp,
							  unsigned int* const calc_thread)
{
  // 1. Thread id is block index * block size + thread offset into the block

  const unsigned int thread_idx = (blockIdx.x * blockDim.x) + threadIdx.x;

  block[thread_idx]       = blockIdx.x;
  blockDimVal[thread_idx] = blockDim.x;
  thread[thread_idx]      = threadIdx.x;

  // 2. calculate warp using built in variable warpSize
 
  warp[thread_idx] = threadIdx.x / warpSize;

  calc_thread[thread_idx] = thread_idx;
}

// 2. some global parameters

const int ARRAY_SIZE = 256;
const int ARRAY_SIZE_IN_BYTES = sizeof(unsigned int) * (ARRAY_SIZE);

// 3. some global variables
//    declare staticaly for arrays of ARRAY_SIZE each

unsigned int cpu_block[ARRAY_SIZE];
unsigned int cpu_blockDimVal[ARRAY_SIZE];
unsigned int cpu_thread[ARRAY_SIZE];
unsigned int cpu_warp[ARRAY_SIZE];
unsigned int cpu_calc_thread[ARRAY_SIZE]; 

// 4. the main function

int main()
{
  // 1. Introduction

  const unsigned int num_blocks  = 4;
  const unsigned int num_threads = 64;

  cout << "====================================================================" << endl;
  cout << " A. I evaluate the following CUDA runtime values:"                    << endl;
  cout << " B. thread_idx = (blockIdx.x * blockDim.x) + threadIdx.x"             << endl;
  cout << " C. block[thread_idx]        = blockIdx.x"                            << endl;
  cout << " D. blockDimVal[thread_idx]  = blockDim.x"                            << endl;
  cout << " E. thread[thread_idx]       = threadIdx.x"                           << endl;
  cout << " F. warp[thread_idx]         = threadIdx.x / warpSize"                << endl;
  cout << " G. calc_thread[thread_idx]  = thread_idx"                            << endl;
  cout << " H. The array size equals to = " << ARRAY_SIZE                        << endl;
  cout << " I. The number of threads equals to = " << num_threads                << endl;
  cout << " J. The number of blocks equal to   = " << num_blocks                 << endl;
  cout << "====================================================================" << endl; 
  cout << endl;
  int sentinel;
  cout << " Enter an integer to start: ";
  cin >> sentinel;

  // 2. declare pointers for GPU based params

  unsigned int* gpu_block;
  unsigned int* gpu_blockDimVal; 
  unsigned int* gpu_thread;
  unsigned int* gpu_warp;
  unsigned int* gpu_calc_thread;

  // 3. declare loop counter for later

  unsigned int i;

  // 4. allocate four arrays on the GPU

  cudaMalloc((void**)&gpu_block,       ARRAY_SIZE_IN_BYTES);
  cudaMalloc((void**)&gpu_blockDimVal, ARRAY_SIZE_IN_BYTES);
  cudaMalloc((void**)&gpu_thread,      ARRAY_SIZE_IN_BYTES);
  cudaMalloc((void**)&gpu_warp,        ARRAY_SIZE_IN_BYTES);
  cudaMalloc((void**)&gpu_calc_thread, ARRAY_SIZE_IN_BYTES);

  // 5. Execute the CUDA kernel

  what_is_my_id<<<num_blocks, num_threads>>>(gpu_block,
                                             gpu_blockDimVal,
                                             gpu_thread,
                                             gpu_warp,
                                             gpu_calc_thread);

  // 6. Copy back the gpu results to the CPU

  cudaMemcpy(cpu_block,       gpu_block,       ARRAY_SIZE_IN_BYTES, cudaMemcpyDeviceToHost);
  cudaMemcpy(cpu_blockDimVal, gpu_blockDimVal, ARRAY_SIZE_IN_BYTES, cudaMemcpyDeviceToHost);
  cudaMemcpy(cpu_thread,      gpu_thread,      ARRAY_SIZE_IN_BYTES, cudaMemcpyDeviceToHost);
  cudaMemcpy(cpu_warp,        gpu_warp,        ARRAY_SIZE_IN_BYTES, cudaMemcpyDeviceToHost);
  cudaMemcpy(cpu_calc_thread, gpu_calc_thread, ARRAY_SIZE_IN_BYTES, cudaMemcpyDeviceToHost);

  // 7. Free the arrays on the GPU as now we are done with them

  cudaFree(gpu_block);
  cudaFree(gpu_blockDimVal);
  cudaFree(gpu_thread);
  cudaFree(gpu_warp);
  cudaFree(gpu_calc_thread);

  // 8. Iterate through the arrays and print

  for (i = 0; i < ARRAY_SIZE; i++)
  {
     cout << "=================================================================" << endl;
     cout << " 1 --> Array index       --> " << i                  << endl;
     cout << " 2 --> Calculated thread --> " << cpu_calc_thread[i] << endl;
     cout << " 3 --> Block             --> " << cpu_block[i]       << endl;
     cout << " 4 --> BlockDimVal       --> " << cpu_blockDimVal[i] << endl;
     cout << " 5 --> Warp              --> " << cpu_warp[i]        << endl;
     cout << " 6 --> Thread            --> " << cpu_thread[i]      << endl;
  }

  cout << endl;
  cout << " Enter an integer to exit: ";
  cin >> sentinel;

  return 0;

}

//=============//
// End of code //
//=============//