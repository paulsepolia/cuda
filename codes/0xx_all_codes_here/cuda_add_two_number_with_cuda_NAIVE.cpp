//===============================//
// Add two number on CUDA device //
// Do that many times            //
//===============================//

#include <iostream>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <ctime>

using namespace std;

// 1. The CUDA kernel function

__global__ void add(int a, int b, int *c)
{
  *c = a + b;   
}

// 2. The main function

int main()
{
  // 1. Introduction

  cout << endl;
  cout << "====================================================================" << endl;
  cout << " A. I add using CUDA two doubles: c = a + b"                          << endl;
  cout << " B. I do the addition times equal to 10E5"                            << endl;
  cout << " C. I use 1 CUDA Grid and 1 CUDA Thread"                              << endl;
  cout << "====================================================================" << endl;
  cout << endl;

  int sentinel;
  cout << " Enter an integer to start the benchmark: ";
  cin >> sentinel;
  cout << endl;

  int c;
  int *dev_c;
  const int I_MAX = 100;
  const int J_MAX = 1000;
  clock_t time_start;
  clock_t time_end;

  time_start = clock();

  for (int j = 1; j <= J_MAX; j++)
  {
    for (int i = 1; i <= I_MAX; i++)
    {
      cudaMalloc((void**)&dev_c, sizeof(int));

      add<<<1,1>>>(2, 7, dev_c);

      cudaMemcpy(&c, dev_c, sizeof(int), cudaMemcpyDeviceToHost);

      cudaFree(dev_c);
    }
  }

  time_end = clock();

  cout << " 1 --> GPU Time used = " << (time_end-time_start)/static_cast<double>(CLOCKS_PER_SEC) << endl;
  cout << " 2 --> CPU result = 2 + 7 = 9 " << endl;
  cout << " 3 --> GPU result = " << c << endl;

  cout << endl;
  cout << " Enter an integer to exit: ";
  cin >> sentinel;
  
  return 0;
}

//=============//
// End of code //
//=============//