//==============================================//
// Copy data from device to host and vice versa //
//==============================================//

// 1 --> includes

#include <iostream>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <ctime>

using namespace std;

// 2 --> cuda kernel code

__global__ void vecManip(double* A, int m, int n)
{
  int row = blockDim.y * blockIdx.y + threadIdx.y;
  int col = blockDim.x * blockIdx.x + threadIdx.x;   
  const double coef = 10.0; 
  const double K_MAX = 10000;  

  for (int k = 0; k < K_MAX; k++)
  {
    if ((row < m) && (col < n))
    { A[n*row+col] = coef * A[n*row+col];
      A[n*row+col] = (1.0/coef) * A[n*row+col];
    }
  }

}

// 3 --> host code

int main()
{
  // 1 --> local parameters

  const int coef       = 10;
  const int m_row      = coef * 1024; 
  const int n_col      = coef * 1024;
  const int total_elem = m_row * n_col;
  const size_t size    = m_row * n_col * sizeof(double); 
 
  // 2 --> host and device variables

  clock_t begin_time;
  clock_t end_time;
  double* h_A;             // host
  double* d_A;             // cuda device
  const int I_MAX = 100000;
  int omp_flag;

  cout << " Do I have to use the CPU OpenMP bench (yes=1/no=0)? ";
  cin >> omp_flag; 

for(int i_bench=0; i_bench < I_MAX; i_bench++)
{ // main benchmark
  h_A = new double [total_elem];

  // 3 --> first line output
 
  cout << "====================================================================" << endl;
  cout << " Vector Size is --> " << total_elem << " -----> " << i_bench+1   << endl;
  cout << "====================================================================" << endl;
  cout << endl;   

  // 4 --> initialize host vector 

  cout << "  1 --> Initialize host vector " << endl; 

  begin_time = clock();

  for (int i = 0; i < m_row; i++)
  {
    for (int j = 0; j < n_col; j++)
	{ h_A[i*n_col+j] = rand()/static_cast<double>(RAND_MAX); }
  }

  end_time = clock();

  double time_spent_1 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
 
  cout << "  2 --> cpu time used --> " << time_spent_1 << endl; 

  cout << "  3 --> before --> h_A[0]            = " << h_A[0] << endl;
  cout << "  4 --> before --> h_A[1]            = " << h_A[1] << endl;
  cout << "  5 --> before --> h_A[total_elem-1] = " << h_A[total_elem-1] << endl;
 
  // 5 --> allocate RAM in cuda device

  cudaMalloc((void**)&d_A, size);

  // 6 --> copy vector from host memory to device memory

  cout << "  6 --> copy RAM from HOST to CUDA" << endl; 

  begin_time = clock();

  cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
  
  end_time = clock();
    
  double time_spent_2 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
   
  cout << "  7 --> cpu time used --> " << time_spent_2 << endl;

  // 6 --> set up cuda process grid
 
  cout << "  8 --> CUDA kernel invoke" << endl;

  begin_time = clock();

  const int threadsPerBlock = 32; // maximum for 2D block of threads 
  const int blocksPerGridCol = ceil(static_cast<double>(n_col)/threadsPerBlock);
  const int blocksPerGridRow = ceil(static_cast<double>(m_row)/threadsPerBlock);
    
  dim3 dimBlock(blocksPerGridCol, blocksPerGridRow, 1);
  dim3 dimGrid(threadsPerBlock, threadsPerBlock, 1);

  //  7 --> invoke cuda kernel

  vecManip<<<dimBlock, dimGrid>>>(d_A, m_row, n_col);

  end_time = clock();
  double time_spent_3 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);
  cout << "  9 --> cpu time used --> " << time_spent_3 << endl;

  //  8 --> Copy result from device memory to host memory
  //        h_C contains the result in host memory

  cout << " 10 --> CUDA calculate the algorithm " << endl;

  begin_time = clock();

  cudaMemcpy(h_A, d_A, size, cudaMemcpyDeviceToHost);
    
  end_time = clock();
  double time_spent_4 = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);

  cout << " 11 --> cpu time used --> " << time_spent_4 << endl;

  cout << " 12 --> some output data " << endl;
  cout << " 13 --> after --> h_A[0]            = " << h_A[0] << endl;
  cout << " 14 --> after --> h_A[1]            = " << h_A[1] << endl;
  cout << " 15 --> after --> h_A[total_elem-1] = " << h_A[total_elem-1] << endl;

  // 10 --> clean cuda RAM 

   if (d_A)
   { cudaFree(d_A); }
   
  // 11 --> reset cuda device

  cudaDeviceReset();

  double time_spent_gpu = time_spent_2 + time_spent_3 + time_spent_4;
  cout << " 16 --> total GPU time --> " << time_spent_2 + time_spent_3 + time_spent_4 << endl;
  cout << " 17 --> Host calculate the algorithm " << endl;

  begin_time = clock();

  const int K_MAX_HOST = 10000; 

  int k, n, m;

  if (omp_flag == 1)
  {
 
  #pragma omp parallel for\
  default(none)\
  private(k,n,m)\
  shared(h_A)               

  for (k = 0; k < K_MAX_HOST; k++)
  {
    for (n=0; n < n_col; n++) 
    {
      for (m=0; m < m_row; m++) 
      {
        if ((m < m_row) && (n < n_col))
        { h_A[n*m_row+n_col] = coef * h_A[n*m_row+n_col];
          h_A[n*m_row+n_col] = (1.0/coef) * h_A[n*m_row+n_col]; }
      }
    }
  }

  end_time = clock();
  double time_spent_cpu = (end_time - begin_time)/static_cast<double>(CLOCKS_PER_SEC);

  cout << " 18 --> total CPU time --> " << time_spent_cpu << endl;
  cout << " 19 --> CPU time / GPU time = Speed up = " 
       << time_spent_cpu / time_spent_gpu << endl;
  }
  else if (omp_flag == 0)
  {
    cout << " no cpu bench " << endl;
  }
  else 
  {
    cout << " NOTHING! " << endl;
  }

  cout << endl << endl;

  // 12 --> clean host RAM

  delete [] h_A;

} // main benchmark
  return 0;   
}

//================//
// End of program //
//================//
