
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <ctime>

using namespace std;

// 1. Global parameters

const int I_MAX = 200;
const int COEF = 220;
const int SIZE = COEF * 1024 * 1024;

// 2. Function: cuda_malloc_test 

float cuda_malloc_test(int size, bool up)
{
  cudaEvent_t start;
  cudaEvent_t stop;
  int *a;
  int *dev_a;
  float elapsedTime;
  cudaError_t cudaStatus;
 
  // a.

  cout << endl;
  cout << " Inside --> cuda_malloc_test " << endl;
  cout << endl;

  cudaStatus = cudaEventCreate(&start);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventCreate(&start) failed!" << endl; 
    return (-1); }
  else
  { cout << "  1 --> Success --> cudaEventCreate(&start)" << endl; }

  // b.

  cudaStatus = cudaEventCreate(&stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventCreate(&stop) failed!" << endl; 
    return (-1); }
  else
  { cout << "  2 --> Success --> cudaEventCreate(&stop)" << endl; }

  // c.

  a = (int*)malloc(size* sizeof(*a));
  cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(*dev_a));
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc for dev_a failed!" << endl; 
    return (-1); }
  else
  { cout << "  3 --> Success --> cudaMalloc for dev_a" << endl; }

  // d.

  cudaStatus = cudaEventRecord(start, 0);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventRecord(start,0) failed!" << endl; 
    return (-1); }
  else
  { cout << "  4 --> Success --> cudaEventRecord(start,0)" << endl; }

  // e.

  for(int i = 0; i < I_MAX; i++)
  {
    if(up)
    { cudaStatus = cudaMemcpy(dev_a, a, size* sizeof(*dev_a), cudaMemcpyHostToDevice); 
      if (cudaStatus != cudaSuccess)
      { cout << " Error! cudaMemcpy from host to device, a -> dev_a, failed!" << endl; 
        return (-1); }
      else if (((i == 0) || (i == I_MAX-1)) && (cudaStatus == cudaSuccess))
      { cout << "  5A --> Success --> cudaMemcpy from host to device, a -> dev_a" << endl; }
    }
    else
    { cudaStatus = cudaMemcpy(a, dev_a, size* sizeof(*dev_a), cudaMemcpyDeviceToHost); 
      if (cudaStatus != cudaSuccess)
      { cout << " Error! cudaMemcpy from host to device, a -> dev_a, failed!" << endl; 
        return (-1); }
      else if (((i == 0) || (i == I_MAX-1)) && (cudaStatus == cudaSuccess))
      { cout << "  5B --> Success --> cudaMemcpy from host to device, a -> dev_a" << endl; }
    }
  }

  // f.

  cudaStatus = cudaEventRecord(stop, 0);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventRecord(stop, 0), failed!" << endl; 
    return (-1); }
  else
  { cout << "  6 --> Success --> cudaEventRecord(stop, 0)" << endl; }

  // g.

  cudaStatus = cudaEventSynchronize(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventSynchronize(stop), failed!" << endl; 
    return (-1); }
  else
  { cout << "  7 --> Success --> cudaEventSynchronize(stop)" << endl; }

  // h.

  cudaStatus = cudaEventElapsedTime(&elapsedTime, start, stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventElapsedTime(&elapsedTime, start, stop), failed!" << endl; 
    return (-1); }
  else
  { cout << "  8 --> Success --> cudaEventElapsedTime(&elapsedTime, start, stop)" << endl; }

  // i.

  free(a);
  
  cudaStatus = cudaFree(dev_a);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaFree(dev_a), failed!" << endl; 
    return (-1); }
  else
  { cout << "  9 --> Success --> cudaFree(dev_a)" << endl; }  

  // j.
  
  cudaStatus = cudaEventDestroy(start);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventDestroy(start), failed!" << endl; 
    return (-1); }
  else
  { cout << " 10 --> Success --> cudaEventDestroy(start)" << endl; }
 
  // k.

  cudaStatus = cudaEventDestroy(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventDestroy(stop), failed!" << endl; 
    return (-1); }
  else
  { cout << " 11 --> Success --> cudaEventDestroy(stop)" << endl; }
 
  // l.

  cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess) 
  { cout << " Error! cudaDeviceReset() failed!" << endl;
    return (-1); }
  else
  { cout << " 12 --> Success --> cudaDeviceReset()" << endl; }
  
  return (elapsedTime/CLOCKS_PER_SEC);
}

// 3. Function: cuda_host_alloc_test

float cuda_host_alloc_test(int size, bool up)
{
  cudaEvent_t start;
  cudaEvent_t stop;
  int *a;
  int *dev_a;
  float elapsedTime;
  cudaError_t cudaStatus;

  // a.

  cout << endl;
  cout << " Inside --> cuda_host_alloc_test" << endl;
  cout << endl;

  cudaStatus = cudaEventCreate(&start);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventCreate(&start) failed!" << endl; 
    return (-1); }
  else
  { cout << " 13 --> Success --> cudaEventCreate(&start)" << endl; }
  
  // b.

  cudaStatus = cudaEventCreate(&stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventCreate(&stop) failed!" << endl; 
    return (-1); }
  else
  { cout << " 14 --> Success --> cudaEventCreate(&stop)" << endl; }

  // c.

  cudaStatus = cudaHostAlloc((void**)&a, size*sizeof(*a), cudaHostAllocDefault);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaHostAlloc for a failed!" << endl; 
    return (-1); }
  else
  { cout << " 15 --> Success --> cudaHostAlloc for a" << endl; }  

  // d.

  cudaStatus = cudaMalloc((void**)&dev_a, size*sizeof(*dev_a));
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaMalloc for dev_a failed!" << endl; 
    return (-1); }
  else
  { cout << " 16 --> Success --> cudaMalloc for dev_a" << endl; }

  // e.
  
  cudaStatus = cudaEventRecord(start,0);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventRecord(start, 0), failed!" << endl; 
    return (-1); }
  else
  { cout << " 17 --> Success --> cudaEventRecord(start, 0)" << endl; }

  // f.

  for(int i = 0; i < I_MAX; i++)
  {
    if(up)
    { cudaStatus = cudaMemcpy(dev_a, a, size* sizeof(*a), cudaMemcpyHostToDevice);
      if (cudaStatus != cudaSuccess)
      { cout << " Error! cudaMemcpy from a to dev_a, failed!" << endl; 
        return (-1); }
      else if ((cudaStatus == cudaSuccess) && (i==I_MAX-1))
     { cout << " 18A --> Success --> cudaMemcpy from a to dev_a" << endl; }
    }
    else
    { cudaStatus = cudaMemcpy(a, dev_a, size* sizeof(*a), cudaMemcpyDeviceToHost); 
      if (cudaStatus != cudaSuccess)
      { cout << " Error! cudaMemcpy from dev_a to a, failed!" << endl; 
        return (-1); }
      else if ((cudaStatus == cudaSuccess) && (i==I_MAX-1))
      { cout << " 18B --> Success --> cudaMemcpy from dev_a to a" << endl; }
    }
  }

  // f.

  cudaStatus = cudaEventRecord(stop, 0);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventRecord(stop, 0), failed!" << endl; 
    return (-1); }
  else
  { cout << " 19 --> Success --> cudaEventRecord(stop, 0)" << endl; }

  // g.

  cudaStatus = cudaEventSynchronize(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventSynchronize(stop), failed!" << endl; 
    return (-1); }
  else
  { cout << " 20 --> Success --> cudaEventSynchronize(stop)" << endl; }

  // h.

  cudaStatus = cudaEventElapsedTime(&elapsedTime, start, stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventElapsedTime(&elapsedTime, start, stop), failed!" << endl; 
    return (-1); }
  else
  { cout << " 21 --> Success --> cudaEventElapsedTime(&elapsedTime, start, stop)" << endl; }

  // i.
  
  cudaFreeHost(a);  
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaFreeHost(a), failed!" << endl; 
    return (-1); }
  else
  { cout << " 22 --> Success --> cudaFreeHost(a)" << endl; }

  // j.

  cudaStatus = cudaFree(dev_a);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaFree(dev_a), failed!" << endl; 
    return (-1); }
  else
  { cout << " 23 --> Success --> cudaFree(dev_a)" << endl; }  

  // k.
  
  cudaStatus = cudaEventDestroy(start);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventDestroy(start), failed!" << endl; 
    return (-1); }
  else
  { cout << " 24 --> Success --> cudaEventDestroy(start)" << endl; }
 
  // l.

  cudaStatus = cudaEventDestroy(stop);
  if (cudaStatus != cudaSuccess)
  { cout << " Error! cudaEventDestroy(stop), failed!" << endl; 
    return (-1); }
  else
  { cout << " 25 --> Success --> cudaEventDestroy(stop)" << endl; }
 
  // m.

  cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess) 
  { cout << " Error! cudaDeviceReset() failed!" << endl;
    return (-1); }
  else
  { cout << " 26 --> Success --> cudaDeviceReset()" << endl; }
  
  return (elapsedTime/CLOCKS_PER_SEC);
}

// 4. The main function


int main()
{
  // 1. Introduction

const int K_MAX = 100;

int sentinel;

cout << "====================================================================" << endl;
cout << " A. I copy Gbytes   = " << SIZE*sizeof(int)/pow(1024,3.0)             << endl;
cout << " B. I do that times = " << I_MAX                                      << endl;
cout << " C. I use 'cudaHostAlloc' and 'malloc' "                              << endl;   
cout << "====================================================================" << endl; 

for (int k = 1; k <= K_MAX; k++)
{ 
  cout << endl;
  if (k==1) 
  { cout << " Enter an integer to start the benchmark: ";
    cin >> sentinel; }

  cout << endl;
  cout << " ---------------------------------------------------------------------> " << k   
       << endl;
  cout << " ---------------------------------------------------------------------> " << k   
       << endl;
  cout << " ---------------------------------------------------------------------> " << k   
       << endl;

  // 2. Local variables

  float elapsedTime;
  double GB = SIZE*sizeof(int)/pow(1024,3.0);

  // 3. CUDA malloc up

  elapsedTime = cuda_malloc_test(SIZE, true);

  cout << endl;
  cout << " D-1. Time using cudaMalloc and malloc         = " 
       << elapsedTime << endl;
  cout << " D-2. GBytes/s during copy up                  = " 
       << I_MAX*GB/elapsedTime << endl;

  // 4. CUDA malloc down

  elapsedTime = cuda_malloc_test(SIZE, false);

  cout << endl;
  cout << " D-3. Time using cudaMalloc and malloc         = "
       << elapsedTime << endl;
  cout << " D-4. GBytes/s during copy down                = "
       << I_MAX*GB/elapsedTime << endl;

  // 5. CUDA host alloc up

  elapsedTime = cuda_host_alloc_test(SIZE, true);

  cout << endl;
  cout << " D-5. Time using cudaHostAlloc and cudaMalloc  = " 
       << elapsedTime << endl;
  cout << " D-6. GBytes/s during copy up                  = " 
       << I_MAX*GB/elapsedTime << endl;
   
  // 6. CUDA host alloc down

  elapsedTime = cuda_host_alloc_test(SIZE, false);

  cout << endl;
  cout << " D-7. Time using cudaHostAlloc and cudaMalloc  = " 
       << elapsedTime << endl;
  cout << " D-8. GBytes/s during copy down                = " 
       << I_MAX*GB/elapsedTime << endl;

}
  // exit

  cout << endl;
  cout << " Enter an integer to exit: ";
  cin >> sentinel;

  return 0;

}

//=============//
// End of code //
//=============//