//=================================//
// DEVICE PROPERTIES IN C++ SYNTAX //
//=================================//

#include <iostream>
#include <cuda_runtime.h>

using namespace std;

int main()
{
  //  1 --> device values

  struct cudaDeviceProp p;
  int device;

  cudaGetDevice(&device);
  cudaGetDeviceProperties(&p, device);

  cout << "------------------------------------------------------------------------------" << endl;
  cout << " CUDA GPU Properties                                                          " << endl;
  cout << "------------------------------------------------------------------------------" << endl;
  cout << endl;

  cout << "  1 --> Name Of Device           --> "  << p.name                           << endl;
  cout << "  2 --> Total Global Memory      --> "  << p.totalGlobalMem    << " Bytes " << endl; 
  cout << "  3 --> Shared Memory Per Block  --> "  << p.sharedMemPerBlock << " Bytes " << endl;
  cout << "  4 --> Registers Per Block      --> "  << p.regsPerBlock                   << endl; 
  cout << "  5 --> Warp Size                --> "  << p.warpSize                       << endl;
  cout << "  6 --> Memory Pitch             --> "  << p.memPitch          << " Bytes " << endl;
  cout << "  7 --> Max Threads Per Block    --> (" << p.maxThreadsDim[0]  << 
	                                         ", "  << p.maxThreadsDim[1]  <<
					                         ", "  << p.maxThreadsDim[2]  << ")"       << endl;
  cout << "  8 --> Max Grid Size            --> (" << p.maxGridSize[0]    << 
	                                         ", "  << p.maxGridSize[1]    <<
					                         ", "  << p.maxGridSize[2]    << ")"       << endl;
  cout << "  9 --> Clock Rate               --> "  << p.clockRate         << " kHz "   << endl;
  cout << " 10 --> Total Constant Memory    --> "  << p.totalConstMem     << " Bytes " << endl;
  cout << " 11 --> Compute Capability       --> "  << p.major << "."      << p.minor   << endl;	  
  cout << " 12 --> Texture Alignment        --> "  << p.textureAlignment               << endl;
  cout << " 13 --> Device Overlap           --> "  << p.deviceOverlap                  << endl;
  cout << " 14 --> MultiProcessor Count     --> "  << p.multiProcessorCount            << endl;
  cout << " 15 --> KernelExecTimeoutEnabled --> "  << p.kernelExecTimeoutEnabled       << endl; 
  cout << " 16 --> Integrated               --> "  << p.integrated                     << endl;      
  cout << " 17 --> Can Map Host Memory      --> "  << p.canMapHostMemory               << endl;
  cout << " 18 --> Compute Mode             --> "  << p.computeMode                    << endl;
  cout << " 19 --> Max Texture1D            --> "  << p.maxTexture1D                   << endl;
  cout << " 20 --> Max Texture2D            --> (" << p.maxTexture2D[0] <<
	                                          ", " << p.maxTexture2D[1] << ")"         << endl; 
  cout << " 21 --> Max Texture3D            --> (" << p.maxTexture3D[0] <<
	                                          ", " << p.maxTexture3D[1] << 
											  ", " << p.maxTexture3D[2] << ")"         << endl;
  cout << " 22 --> Max Texture1DLayered     --> (" << p.maxTexture1DLayered[0]         <<
	                                          ", " << p.maxTexture1DLayered[1] << ")"  << endl; 
  cout << " 23 --> Max Texture2DLayered     --> (" << p.maxTexture2DLayered[0] <<
	                                          ", " << p.maxTexture2DLayered[1] << 
											  ", " << p.maxTexture2DLayered[2] << ")"  << endl;
  cout << " 24 --> Surface Alignment        -->  " << p.surfaceAlignment               << endl;
  cout << " 25 --> Concurrent Kernels       -->  " << p.concurrentKernels              << endl; 
  cout << " 26 --> ECCEnabled               -->  " << p.ECCEnabled                     << endl;
  cout << " 27 --> PCI BUS ID               -->  " << p.pciBusID                       << endl;
  cout << " 28 --> PCI Device ID            -->  " << p.pciDeviceID                    << endl;
  cout << " 29 --> PCI Domain ID            -->  " << p.pciDomainID                    << endl;
  cout << " 30 --> tcc Driver               -->  " << p.tccDriver                      << endl;
  cout << " 31 --> Asynch Engine Count      -->  " << p.asyncEngineCount               << endl;
  cout << " 32 --> Unified Addressing       -->  " << p.unifiedAddressing              << endl;
  cout << " 33 --> Memory Clock Rate        -->  " << p.memoryClockRate << " KHz "     << endl;
  cout << " 34 --> Memory Bus Width         -->  " << p.memoryBusWidth << " bits "     << endl;
  cout << " 35 --> L2 Cache Size            -->  " << p.l2CacheSize << " Bytes "       << endl;
  cout << " 36 --> MaxThreadsPerMultiP      -->  " << p.maxThreadsPerMultiProcessor    << endl;              

  cout << endl;
  cout << " some adjustable parameters " << endl;
  cout << endl;

  size_t limit = 0;
 
  cudaDeviceGetLimit(&limit, cudaLimitStackSize);
  cout << " 37 --> cudaLimitStackSize      --> " << limit << endl;
 
  cudaDeviceGetLimit(&limit, cudaLimitPrintfFifoSize);
  cout << " 38 --> cudaLimitPrintfFifoSize --> " << limit << endl;

  cudaDeviceGetLimit(&limit, cudaLimitMallocHeapSize);
  cout << " 39 --> cudaLimitMallocHeapSize --> " << limit << endl;

  int sentinel;
  cout << " Enter an integer to exit:";
  cin >> sentinel;

  return 0;   
}

//================//
// End of program //
//================//