//============================//
// Get the device properties  //
//============================//

#include <iostream>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

using namespace std;

int main()
{
  cudaDeviceProp prop;
  int dev;

  cudaGetDevice(&dev);
  cout << " ID of current CUDA device: " << dev << endl;

  memset(&prop, 0, sizeof(cudaDeviceProp));

  prop.major = 1;
  prop.minor = 3;

  cudaChooseDevice(&dev, &prop);

  cout << " ID of CUDA device closest to revision 1.3: " << dev << endl;

  int sentinel;
  cout << " Enter an integer to exit:";
  cin >> sentinel;

  return 0;
}

//=============//
// End of code //
//=============//